import pygame 
import time
import random
import serial
from UserList import UserList
import main

pygame.init()
##### Parameters
white  = (255, 255, 255)    
info = pygame.display.Info()
resolution = (info.current_w,info.current_h)

ser = serial.Serial('/dev/ttyACM0')
ser.baudrate = 9600

#####  classes and functions
class Live_item():
    default_img = "images/galop#"
    default_scale_factor = 0.4

    def __init__(self, x, y, img = default_img, nr_img = 3, scale_factor=default_scale_factor):
        self.nr_img = nr_img
        self.scale_factor = scale_factor        
        if nr_img > 1:
            self.img_list = [ self.scale( pygame.image.load(img + "%i.png" % i), self.scale_factor ) for i in range(1, nr_img + 1) ]
        else :
            self.img_list = [ self.scale( pygame.image.load( img ) , self.scale_factor  ) ]
        self.anim_nr1 = 0
        self.anim_nr2 = 0
        self.width, self.height = self.dimentions(self.img_list[0])
        if x == 0.0:
            self.x = resolution[0] * .8
            self.y = random.randrange( 0, resolution[1] - 40 - self.height )
        else:
            self.x = x
            self.y = y
            
    def scale(self, image, scale):
        size = self.dimentions(image)
        image = pygame.transform.smoothscale(image, (int(size[0]*scale), int(size[1]*scale)))    
        return image

    def dimentions(self, image):
        self.w = image.get_rect().size[0]
        self.h = image.get_rect().size[1]
        return self.w, self.h

    def animate(self):
        if self.nr_img == 1:
            self.current_img = self.img_list[0]
        else:
            self.anim_nr1 += 1
            if self.anim_nr1 >= 5:
                self.anim_nr2 +=1
                self.anim_nr1 = 0
            self.anim_nr2 = self.anim_nr2 % len(self.img_list)
            self.current_img = self.img_list[self.anim_nr2]
        self.display()

    def display(self):  
        gameDisplay.blit(self.current_img,(self.x, self.y))        


class Wolf(Live_item):
    default_x = resolution[0] * .8
    default_y = random.randrange( 0, resolution[1]-40 )
    def __init__(self, x=0.0, y=0.0):
        img = "images/wolf#"
        scale_factor = 0.7
        Live_item.__init__(self, x, y, img, 4, scale_factor)
        self.speed = 7

    def run(self):
        self.x -= self.speed


class Goodies(Live_item):
    default_x = random.randrange( 0, resolution[0] )
    default_y = random.randrange( 0, resolution[1] - 40 )
    def __init__(self, x=default_x, y=default_y):
        img = "images/apple.png"
        scale_factor = 0.05
        Live_item.__init__(self, x, y, img, scale_factor)
        self.speedy = 2

    def run(self):
        self.x -= self.speedy

class Pig(Live_item):
    default_x = resolution[0] * 0.02
    default_y = resolution[1] * 0.5

    def __init__(self, x=default_x, y=default_y):
        img = "images/galop#"
        scale_factor = 0.3
        Live_item.__init__(self, x, y, img, 3, scale_factor)
        self.crashed = False
        self.apples  = 0
        self.lives   = 2000

    def crash_check(self, a_list):
        for obj in a_list:
            if obj.x < self.x + self.width:
                ears  =  obj.y  <= self.y <= obj.y + obj.height
                hoofs =  obj.y  <= self.y + self.height <= obj.y + obj.height
                if ears or hoofs:
                    return obj
                else:
                    return None

    def is_crashed(self, wolves, apples, background):
#        for wolf in wolves:
#            if wolf.x < self.x + self.width:
#                ears  =  wolf.y  <= self.y <= wolf.y + wolf.height
#                hoofs =  wolf.y  <= self.y + self.height <= wolf.y + wolf.height
#                if ears or hoofs:
        wolf = self.crash_check(wolves)
        if wolf:
            self.current_img = self.scale( pygame.image.load("images/crashed_pig.png"), self.scale_factor  )
            self.lives -= 1
            background.update()            
            pygame.draw.rect(gameDisplay, (255,255,255), [0, resolution[1]-40, resolution[0], 40])
            self.display()
            wolves.remove(wolf)
            pygame.display.update()
            wolves.refresh()
            time.sleep(2)
            return

        apple = self.crash_check(apples)
        if apple:
            print "crounch"
            apple_sound.set_volume(0.8)
            apple_sound.play()
            self.apples += 1
            apples.remove(apple)


class Background():
    def __init__(self, image, scrollspeed = 6):
        self.image = image
        self.scrollspeed = scrollspeed
        self.bgnd_x = 0 
        self.bgnd1 = self.choose_images(self.image)        
        self.bgnd2 = self.choose_images(self.image)        

    def choose_images(self, image):
        bg_nr = random.randint(1,3)                                                  
        bgnd = pygame.image.load("./images/%s#%i.png" %(image, bg_nr))
        bgnd = pygame.transform.smoothscale(bgnd, resolution)   
        return bgnd
        
    def update(self, image=None):
        if not image:
            image = self.image

            if self.bgnd_x <= -resolution[0]:
                self.bgnd_x = 0 
                self.bgnd1  =  self.bgnd2 
                self.bgnd2 = self.choose_images(image)  
        else :
            if self.bgnd_x <= -resolution[0]:
                self.bgnd_x = 0 
                self.bgnd1  =  self.bgnd2 
                self.bgnd2 = pygame.image.load("./images/" + image )

                if self.bgnd_x <= -2 * resolution[0]:
                    self.bgnd1  =  self.bgnd2 
                    self.bgnd2 = pygame.image.load("./images/" + image )
                    self.bgnd2 = pygame.transform.smoothscale(self.bgnd2, resolution) 
                    go = Background("./images/" + image , 0)
                    go.update()
                    time.sleep(10.0)
                    
        gameDisplay.blit(self.bgnd1, [self.bgnd_x, 0])
        gameDisplay.blit(self.bgnd2, [self.bgnd_x + resolution[0], 0])
        self.bgnd_x -= self.scrollspeed
        
            
            

class Farm(UserList):
    def __init__(self, crop = "wolf", pack_size = 2):
        UserList.__init__(self)
        self.crop = crop
        self.pack_size = pack_size
        self.dodged = 0

    def remove_wolf(self):
        for wolf in self:
            if wolf.x < 0:
                self.remove(wolf)
                self.dodged += 1

    def add_wolf(self):
        if len(self) < self.pack_size:
            if random.random()<.03:
              self.new_wolf()
              
    def new_wolf(self):
        if self.crop == "wolf":
            obj = Wolf(0.0, 0.0)
        elif self.crop == "apple":
            obj = Goodies(0.0, 0.0)
            
        overlapx = [ i.x < obj.x < i.x + i.width for i in self]
        overlapy = [ i.y < obj.y < i.y + i.height for i in self]
        if not any(overlapx + overlapy):
            self.append(obj)
    
    def refresh(self):
        self.add_wolf()
        self.remove_wolf()
        for wolf in self:
            wolf.run()
            wolf.animate()




def message_display(text, x, y, fs=16, color=(0,0,0)):
    # initialize font; must be called after 'pygame.init()' to avoid 'Font not Initialized' error
    myfont = pygame.font.SysFont("monospace", fs)

    # render text
    label = myfont.render(text, 1, color)
    gameDisplay.blit(label, (x, y))


def game_loop():
    pig = Pig() 
    wolf_farm = Farm("wolf", 3)
    apple_farm = Farm("apple", 2)

    background = Background("grass_bg", 2)

    game_exit = False
    bike = 0.0
    while not game_exit :

        if ser.inWaiting() > 0:
            
            bike = ser.readline()
            ser.flush()
            ser.flushInput() 
            ser.flushOutput()
            
            try:
                bike = float(bike)
            except ValueError:
                print "ERROR : ", bike
                bike = 0.0

        speed_thres = 65.0
        speed_diff = abs(bike - speed_thres)
        speed_factor = 0.00009 * (4 * speed_diff)**0.5
        

        # moves pig up if bike        
        if bike > speed_thres:
            if not pig.y < pig.height/4 :
                pig.y -= resolution[1] * speed_factor
                old_time = time.time()
                 
        if bike < speed_thres:
            if not pig.y > resolution[1] - 40 - pig.height:
                pig.y += resolution[1] * speed_factor
                old_time = time.time() 
                    
        
        for event in pygame.event.get():    
            if event.type == pygame.QUIT:
                game_exit = True
            
            if event.type == pygame.KEYDOWN:

                ## Allows shifting  between fullscreen and window mode
                if event.key == pygame.K_w:
                    pygame.display.set_mode(resolution)
                if event.key == pygame.K_f:
                    pygame.display.set_mode(resolution, pygame.FULLSCREEN)
                    
                if event.key == pygame.K_ESCAPE:
                    game_exit = True
                if event.key == pygame.K_q:
                    game_exit = True

##        if pig.apples > 0 :
##            background.update("grass_end.png")
##        else:
        background.update()
            
        pygame.draw.rect(gameDisplay, (255,255,255), [0, resolution[1]-40, resolution[0], 40])
        pig.animate()

        pig.is_crashed(wolf_farm, apple_farm, background)
        if pig.lives == 0 :
#             pygame.quit()
            game_exit = True
#            main.menu()            
#            gm = main.GameMenu(gameDisplay, funcs.keys(), funcs)
#            gm.run()

        apple_farm.refresh()
        wolf_farm.refresh()

        message_display("Dodged wolves : %i" % wolf_farm.dodged, resolution[0]*0.2, 
                                                        resolution[1]*0.96, fs = 30, color = (0, 255, 0) )
        message_display("Apples : %i" % pig.apples, resolution[0]*0.4, resolution[1]*0.96, fs = 30, color = (0, 0, 255) )
        message_display("Lives : %i" % pig.lives, resolution[0]*0.6, resolution[1]*0.96, fs = 30, color = (255, 0, 0) )
 
        pygame.display.update()
#        clock.tick(120)

#    go = pygame.image.load("images/gameover.png")
#    gameDisplay.blit(go, (0, 0))
    go = Background("images/gameover.png", 0)
    go.update()
    pygame.display.update()
    time.sleep(2.5)
    main.menu()   

def main_loop():
        
    global gameDisplay, clock
    global apple_sound

    gameDisplay = pygame.display.set_mode( resolution )
#    gameDisplay = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)
    pygame.display.set_caption("Pyg Race!")#("Vas y mon cochon!!")
    clock = pygame.time.Clock()

    pygame.mixer.init()
    apple_sound = pygame.mixer.Sound('sounds/pomme.wav')
    #pygame.mixer.music.load('assets/sounds/OGG/farm.ogg')
    #pygame.mixer.music.play(-1)

    game_loop()
    pygame.quit()

#######   Main Loop  ###
if __name__ == "__main__":
    main_loop()




