#!/usr/bin/python
import sys
import pygame
from collections import OrderedDict

import cochon4

pygame.init()
 
WHITE = (255, 255, 255)
RED = (255, 0, 0)
BLACK = (0, 0, 0)
GREY = (127,127,127)
BLUE = (0,0,255)
 
#global gameDisplay, resolution
#global difficulty

info = pygame.display.Info()
resolution = (info.current_w,info.current_h)
difficulty = 1



class Background():
    def __init__(self, image = None, scrollspeed = 6):
        self.scrollspeed = scrollspeed
        self.grass = pygame.image.load(image)
        self.grass = pygame.transform.smoothscale(self.grass, resolution)   
        self.grass_x = 0 

    def update(self):
        gameDisplay.blit(self.grass, [self.grass_x, 0])
        gameDisplay.blit(self.grass, [self.grass_x + resolution[0], 0])
        self.grass_x -= self.scrollspeed
        if self.grass_x <= -resolution[0]:
            self.grass_x = 0 


class MenuItem(pygame.font.Font):
    def __init__(self, text, font=None, font_size=30,
                 font_color=WHITE, (pos_x, pos_y)=(0, 0)):
 
        pygame.font.Font.__init__(self, font, font_size)
        self.text = text
        self.font_size = font_size
        self.font_color = font_color
        self.label = self.render(self.text, 1, self.font_color)
        self.width = self.label.get_rect().width
        self.height = self.label.get_rect().height
        self.dimensions = (self.width, self.height)
        self.pos_x = pos_x
        self.pos_y = pos_y
        self.position = pos_x, pos_y
 
    def set_position(self, x, y):
        self.position = (x, y)
        self.pos_x = x
        self.pos_y = y
 
    def set_font_color(self, rgb_tuple):
        self.font_color = rgb_tuple
        self.label = self.render(self.text, 1, self.font_color)
 

class GenericMenu():
    def __init__(self, font_color):
        self.screen = gameDisplay
        self.scr_width = self.screen.get_rect().width
        self.scr_height = self.screen.get_rect().height
        self.font_color = font_color

        self.screen.fill(WHITE)
        self.background = Background("images/menu_bkg2.png", 0)
        self.ear = pygame.image.load("images/pig_ear.png")

        self.items = []
        self.non_menu_items = []

    def set_keyboard_selection(self, key):
        """
        Marks the MenuItem chosen via up and down keys.
        """
        # Return all to unselected
        for item in self.items:
            item.set_italic(False)
            item.set_font_color(self.font_color)
 
        # Find the selected item
        if self.cur_item is None:
            self.cur_item = 0
        else:
            if key == pygame.K_UP and \
                    self.cur_item > 0:
                self.cur_item -= 1
            elif key == pygame.K_UP and \
                    self.cur_item == 0:
                self.cur_item = len(self.items) - 1
            elif key == pygame.K_DOWN and \
                    self.cur_item < len(self.items) - 1:
                self.cur_item += 1
            elif key == pygame.K_DOWN and \
                    self.cur_item == len(self.items) - 1:
                self.cur_item = 0
 
        self.background.update()
        self.items[self.cur_item].set_italic(True)
        self.items[self.cur_item].set_font_color(RED)

        x = self.items[self.cur_item].pos_x + self.items[self.cur_item].dimensions[0]
        y = self.items[self.cur_item].pos_y -self.items[self.cur_item].dimensions[1] * 0.35
        self.screen.blit(self.ear, (x, y)) 
 
        # Finally check if Enter or Space is pressed
        if key == pygame.K_SPACE or key == pygame.K_RETURN:
            text = self.items[self.cur_item].text
            self.funcs[text]()
 

class GameMenu(GenericMenu):
    def __init__(self, gameDisplay, 
                    items, funcs, font=None, 
                        font_size=90, font_color=BLACK ):
                        
        GenericMenu.__init__(self, font_color)
        self.ear = pygame.transform.smoothscale(self.ear, (int(self.ear.get_rect().size[0]*0.5), int(self.ear.get_rect().size[1]*0.5) ) )
        self.clock = pygame.time.Clock()
 
        self.funcs = funcs
        self.background.update()

        for index, item in enumerate(items):
            menu_item = MenuItem(item, font, font_size, self.font_color)
 
            # t_h: total height of text block
            t_h = len(items) * menu_item.height
            pos_x = (self.scr_width / 3) - (menu_item.width / 2)

            pos_y = (self.scr_height / 3) - (t_h / 2) + ((index*2) + index * menu_item.height)
 
            menu_item.set_position(pos_x, pos_y)
            self.items.append(menu_item)

        self.cur_item = None


    def run(self):
        print difficulty
        mainloop = True
        while mainloop:
            # Limit frame speed to 20 FPS
            self.clock.tick(20)

            mpos = pygame.mouse.get_pos()
 
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    mainloop = False
                if event.type == pygame.KEYDOWN:
                    self.background.update()
                    self.set_keyboard_selection(event.key)
 
            for item in self.items:
                self.screen.blit(item.label, item.position)

            pygame.display.flip()



class CreditsMenu(GenericMenu):
    def __init__(self, gameDisplay):

        GenericMenu.__init__(self, BLACK)

        self.ear = pygame.transform.smoothscale(self.ear, (int(self.ear.get_rect().size[0]*0.5), int(self.ear.get_rect().size[1]*0.5) ) )
        self.clock = pygame.time.Clock()
 
        self.funcs = { "Back to Menu": menu}


        back_item = MenuItem(self.funcs.keys()[0],font=None,font_size=90)

        back_item.set_position(375., 820.)
        self.items.append(back_item)

        font = "/usr/share/fonts/truetype/tlwg/TlwgMono.ttf"

        ######   CRACK !!
        text_item = MenuItem("Crack : original concept", font, 50, (0,0,0), (500, 240) ) 
        self.non_menu_items.append(text_item)
        text_item = MenuItem("Programmation", font, 50, (0,0,0), (660, 290) ) 
        self.non_menu_items.append(text_item)
        text_item = MenuItem("Design and Drawing", font, 50, (0,0,0), (590, 340) )
        self.non_menu_items.append(text_item)

        ######   CRICK !!
        text_item = MenuItem("Crick : Apple sounds", font, 50, (0,0,0), (200, 530) )
        self.non_menu_items.append(text_item)
        text_item = MenuItem("Muse", font,50,(0,0,0),(450, 580) )
        self.non_menu_items.append(text_item)
        text_item = MenuItem("Mother of all Pigs",font,50,(0,0,0), (230, 630) )
        self.non_menu_items.append(text_item)

        self.background.update()
        self.cur_item = None


    def run(self):
        mainloop = True
        while mainloop:
            # Limit frame speed to 20 FPS
            self.clock.tick(20)

            mpos = pygame.mouse.get_pos()
 
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    mainloop = False
                if event.type == pygame.KEYDOWN:
                    self.background.update()
                    self.set_keyboard_selection(event.key)
 
            for item in self.items + self.non_menu_items:
                self.screen.blit(item.label, item.position)

            img = pygame.image.load("images/crack.png")
            image = pygame.transform.smoothscale(img, (int(img.get_rect().size[0]*0.25), int(img.get_rect().size[1]*0.25)) )
            self.screen.blit(image, (320., 250.)) 
 
            img = pygame.image.load("images/crick.png")
            image = pygame.transform.smoothscale(img, (int(img.get_rect().size[0]*0.25), int(img.get_rect().size[1]*0.25)) )
            self.screen.blit(image, (820., 520.)) 

            pygame.display.flip()



class OptionsMenu(GenericMenu):

    def __init__(self, gameDisplay, font=None, 
                        font_size=90, font_color=BLACK):
                        
        GenericMenu.__init__(self, BLACK)
        self.ear = pygame.transform.smoothscale(self.ear, (int(self.ear.get_rect().size[0]*0.5), int(self.ear.get_rect().size[1]*0.5) ) )
        self.clock = pygame.time.Clock()
 
        self.funcs = OrderedDict([ 
             ('Fiddler PyG' 
             , self.set_difficulty),
             ('Practical Pyg' 
             , self.set_difficulty),
             ('Big Bad wolf'
             , self.set_difficulty),
              ("Back to Menu", menu) ])
   
        ## 
        title_font = "/usr/share/fonts/truetype/tlwg/TlwgMono.ttf"
        diff_text = MenuItem("Difficulty levels", title_font, 50, (0,0,0), (280, 120) ) 
        self.non_menu_items.append(diff_text)
        self.non_menu_items.append(diff_text)
        self.non_menu_items.append(diff_text)
        ######   CRACK !!
        text_item = MenuItem("Crack : original concept", font, 50, (0,0,0), (500, 240) ) 
        self.non_menu_items.append(text_item)
        ##  Sets menu items for diff levels
        for index, item in enumerate(self.funcs.keys()[:-1]):
            menu_item = MenuItem(item, font, font_size, self.font_color)
 
            # t_h: total height of text block
            t_h = len(self.funcs.keys()) * menu_item.height
            pos_x = (self.scr_width / 3) - (menu_item.width / 2)
            pos_y = (self.scr_height / 3) + 50  - (t_h / 2) + ((index*2) + index * (60 + menu_item.height) )
 
            menu_item.set_position(pos_x, pos_y)
            self.items.append(menu_item)
            
        ###  sets "back to menu" item
        menu_item = MenuItem(self.funcs.keys()[-1], font, font_size, BLACK)
        pos_x = (self.scr_width / 3) - (menu_item.width / 2)
        pos_y = (self.scr_height / 3) - (t_h / 2) + ((index*2) + index * 4 * menu_item.height)
        menu_item.set_position(pos_x, pos_y)
        self.items.append(menu_item)
        
        self.background.update()
        self.cur_item = difficulty
        self.set_difficulty()
        
     
    def run(self):
        mainloop = True
        while mainloop:
            # Limit frame speed to 20 FPS
            self.clock.tick(20)

            mpos = pygame.mouse.get_pos()
 
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    mainloop = False
                if event.type == pygame.KEYDOWN:
                    self.background.update()
                    self.set_keyboard_selection(event.key)
            
            for item in self.items + self.non_menu_items:
                self.screen.blit(item.label, item.position)

            pygame.display.flip()
    
    def set_difficulty(self):
        global difficulty  
        difficulty = self.cur_item  
        x = self.items[self.cur_item].pos_x
        y = self.items[self.cur_item].pos_y

        font = "/usr/share/fonts/truetype/tlwg/TlwgMono.ttf"
        if self.cur_item == 0:
            text_item = MenuItem("Yeah take it easy...", font, 50, (0,0,0), (x, y+55) ) 
        elif self.cur_item == 1:
            text_item = MenuItem("Play by the rules.", font, 50, (0,0,0), (x, y+55) )         
        elif self.cur_item == 2:        
            text_item = MenuItem("Are you sure this is for you?", font, 50, (0,0,0), (x, y+55) )  
        
        selection_item = MenuItem(self.items[self.cur_item].text, None, 90, BLUE, (x,y))  
        selection_item.set_italic(True)

#        self.non_menu_items.append()
        self.non_menu_items[-2] = text_item
        self.non_menu_items[-1] = selection_item


def credits():
    pygame.display.set_caption('PyG!')
    cm = CreditsMenu(gameDisplay)
    cm.run()

def options():
    om = OptionsMenu(gameDisplay)
    om.run()
    
def scores():
    print "Hello World!"

    
def menu(): 
    funcs = OrderedDict([ ('Play', cochon4.main_loop),
             ('Credits', credits),
             ('Options', options),
             ('Best Scores', scores),
             ('Quit', sys.exit) ])
 
    pygame.display.set_caption('PyG!')
    gm = GameMenu(gameDisplay, funcs.keys(), funcs)
    gm.run()


if __name__ == "__main__":
    # Creating the screen
    gameDisplay = pygame.display.set_mode((0, 0))#, pygame.FULLSCREEN)
    menu()

