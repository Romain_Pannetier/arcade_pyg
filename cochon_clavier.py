import pygame 
import time
import random
from UserList import UserList
import main

pygame.init()
##### Parameters
resolution = (1700,1000)
white  = (255, 255, 255)    

#####  classes and functions
class Live_item():
    default_img = "images/galop#"
    default_scale_factor = 0.4

    def __init__(self, x, y, img = default_img, nr_img = 3, scale_factor=default_scale_factor):
        self.x = x
        self.y = y
        self.nr_img = nr_img
        self.scale_factor = scale_factor        
        if nr_img > 1:
            self.img_list = [ self.scale( pygame.image.load(img + "%i.png" % i), self.scale_factor ) for i in range(1, nr_img + 1) ]
        else :
            self.img_list = [ self.scale( pygame.image.load( img ) , self.scale_factor  ) ]
        self.anim_nr1 = 0
        self.anim_nr2 = 0
        self.width, self.height = self.dimentions(self.img_list[0])

    def scale(self, image, scale):
        size = self.dimentions(image)
        image = pygame.transform.smoothscale(image, (int(size[0]*scale), int(size[1]*scale)))    
        return image

    def dimentions(self, image):
        self.w = image.get_rect().size[0]
        self.h = image.get_rect().size[1]
        return self.w, self.h

    def animate(self):
        if self.nr_img == 1:
            self.current_img = self.img_list[0]
        else:
            self.anim_nr1 += 1
            if self.anim_nr1 >= 5:
                self.anim_nr2 +=1
                self.anim_nr1 = 0
            self.anim_nr2 = self.anim_nr2 % len(self.img_list)
            self.current_img = self.img_list[self.anim_nr2]
        self.display()

    def display(self):  
        gameDisplay.blit(self.current_img,(self.x, self.y))        


class Wolf(Live_item):
    default_x = resolution[0] * .8
    default_y = random.randrange( 0, resolution[1]-40 )
    def __init__(self, x=default_x, y=default_y):
        img = "images/wolf#"
        scale_factor = 0.7
        Live_item.__init__(self, x, y, img, 4, scale_factor)
        self.speed = 7

    def run(self):
        self.x -= self.speed


class Goodies(Live_item):
    default_x = random.randrange( 0, resolution[0] )
    default_y = random.randrange( 0, resolution[1]-40 )

    def __init__(self, x=default_x, y=default_y):
        img = "images/apple.png"
        scale_factor = 0.05
        Live_item.__init__(self, x, y, img, scale_factor)
        self.speedy = 2

    def run(self):
        self.x -= self.speedy

class Pig(Live_item):
    default_x = resolution[0] * 0.02
    default_y = resolution[1] * 0.5

    def __init__(self, x=default_x, y=default_y):
        img = "images/galopmarin#"
        scale_factor = 0.3
        Live_item.__init__(self, x, y, img, 3, scale_factor)
        self.crashed = False
        self.apples  = 0
        self.lives   = 2

    def crash_check(self, a_list):
        for obj in a_list:
            if obj.x < self.x + self.width:
                ears  =  obj.y  <= self.y <= obj.y + obj.height
                hoofs =  obj.y  <= self.y + self.height <= obj.y + obj.height
                if ears or hoofs:
                    return obj
                else:
                    return None

    def is_crashed(self, wolves, apples, background):
#        for wolf in wolves:
#            if wolf.x < self.x + self.width:
#                ears  =  wolf.y  <= self.y <= wolf.y + wolf.height
#                hoofs =  wolf.y  <= self.y + self.height <= wolf.y + wolf.height
#                if ears or hoofs:
        wolf = self.crash_check(wolves)
        if wolf:
            self.current_img = self.scale( pygame.image.load("images/crashed_pig.png"), self.scale_factor  )
            self.lives -= 1
            background.update()            
            pygame.draw.rect(gameDisplay, (255,255,255), [0, resolution[1]-40, resolution[0], 40])
            self.display()
            wolves.remove(wolf)
            pygame.display.update()
            wolves.refresh()
            time.sleep(2)
            return

        apple = self.crash_check(apples)
        if apple:
            print "crounch"
            apple_sound.set_volume(0.8)
            apple_sound.play()
            self.apples += 1
            apples.remove(apple)

class Background():
    def __init__(self, image = None, scrollspeed = 6):
        self.scrollspeed = scrollspeed
        self.grass = pygame.image.load(image)
        self.grass = pygame.transform.smoothscale(self.grass, resolution)   
        self.grass_x = 0 

    def update(self):
        gameDisplay.blit(self.grass, [self.grass_x, 0])
        gameDisplay.blit(self.grass, [self.grass_x + resolution[0], 0])
        self.grass_x -= self.scrollspeed
        if self.grass_x <= -resolution[0]:
            self.grass_x = 0 

class Farm(UserList):
    def __init__(self, crop = "wolf", pack_size = 2):
        UserList.__init__(self)
        self.crop = crop
        self.pack_size = pack_size
        self.dodged = 0

    def remove_wolf(self):
        for wolf in self:
            if wolf.x < 0:
                self.remove(wolf)
                self.dodged += 1

    def add_wolf(self):
        if len(self) < self.pack_size:
            if random.random()<.03:
                x = resolution[0] * .8
                y = random.randrange( 0, resolution[1]-40 )
                overlapx = [ i.x < x < i.x + i.width for i in self]
                overlapy = [ i.y < y < i.y + i.height for i in self]
                if not any(overlapx + overlapy):
                    if self.crop == "wolf":
                        obj = Wolf(x, y)
                    elif self.crop == "apple":
                        obj = Goodies(x, y)
                    self.append(obj)
    
    def refresh(self):
        self.add_wolf()
        self.remove_wolf()
        for wolf in self:
            wolf.run()
            wolf.animate()





def message_display(text, x, y, fs=16, color=(0,0,0)):
    # initialize font; must be called after 'pygame.init()' to avoid 'Font not Initialized' error
    myfont = pygame.font.SysFont("monospace", fs)

    # render text
    label = myfont.render(text, 1, color)
    gameDisplay.blit(label, (x, y))



def game_loop():

    background = Background("images/grass3.jpeg", 2)
    pig = Pig() 
    wolf_farm = Farm("wolf", 3)
    apple_farm = Farm("apple", 2)

    game_exit = False
    
    while not game_exit :
        for event in pygame.event.get():    
            if event.type == pygame.QUIT:
                game_exit = True
            
            if event.type == pygame.KEYDOWN:

                if event.key == pygame.K_ESCAPE:
                    game_exit = True
                if event.key == pygame.K_q:
                    game_exit = True
                
                stride = (resolution[1]-40) * 0.02
                if event.key == pygame.K_UP:
                    if pig.y > 0 :
                        pig.y -= stride

                if event.key == pygame.K_DOWN:
                    print pig.height, pig.y
                    if  pig.y < resolution[1] - 40 - pig.height:
                        pig.y += stride

        background.update()
        pygame.draw.rect(gameDisplay, (255,255,255), [0, resolution[1]-40, resolution[0], 40])
        pig.animate()

        pig.is_crashed(wolf_farm, apple_farm, background)
        if pig.lives == 0 :
#             pygame.quit()
            game_exit = True
#            main.menu()            
#            gm = main.GameMenu(gameDisplay, funcs.keys(), funcs)
#            gm.run()

        apple_farm.refresh()
        wolf_farm.refresh()

        message_display("Dodged wolves : %i" % wolf_farm.dodged, resolution[0]*0.2, 
                                                        resolution[1]*0.96, fs = 30, color = (0, 255, 0) )
        message_display("Apples : %i" % pig.apples, resolution[0]*0.4, resolution[1]*0.96, fs = 30, color = (0, 0, 255) )
        message_display("Lives : %i" % pig.lives, resolution[0]*0.6, resolution[1]*0.96, fs = 30, color = (255, 0, 0) )
 
        pygame.display.update()
#        clock.tick(120)

#    go = pygame.image.load("images/gameover.png")
#    gameDisplay.blit(go, (0, 0))
    go = Background("images/gameover.png", 0)
    go.update()
    pygame.display.update()
    time.sleep(2)
    main.menu()   

def main_loop():
        
    global gameDisplay, clock
    global apple_sound
    
#    gameDisplay = pygame.display.set_mode( resolution )
    gameDisplay = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)
    pygame.display.set_caption("Pyg Race!")#("Vas y mon cochon!!")
    clock = pygame.time.Clock()

    pygame.mixer.init()
    apple_sound = pygame.mixer.Sound('sounds/pomme.wav')
    #pygame.mixer.music.load('assets/sounds/OGG/farm.ogg')
    #pygame.mixer.music.play(-1)

    game_loop()
    pygame.quit()

#######   Main Loop  ###
if __name__ == "__main__":
    main_loop()




